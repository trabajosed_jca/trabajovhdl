library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fsm is
    Port ( 
        clk: in std_logic;
        reset: in std_logic;
        x: in std_logic;
        parity: out std_logic        
    );
end fsm;

architecture Behavioral of fsm is
    type state_type is (S0, S1);
    signal state, next_state: state_type;
    signal new_clk: std_logic:='0';
    signal cont: integer:=0;
begin
    divisor_frecuencia: process (clk)
    begin
        if rising_edge(clk) then
            cont <= cont+1;
            if cont=50000000 then
                new_clk <= not new_clk;
                cont <= 0;
            end if;
        end if;
    end process;
    sync_proc: process (new_clk)
    begin
        if rising_edge(new_clk) then
            if (reset = '1') then
                state <= S0;
            else
                state <= next_state;
            end if;
        end if;
    end process;
    output_decode: process (state)
    begin
        case (state) is
            when S0 => parity <= '0';
            when S1 => parity <= '1';
            when others => parity <= '0';
        end case;
    end process;
    next_state_decode: process (state, x)
    begin
        next_state <= S0;
        case (state) is
            when S0 =>
                IF (x = '1') THEN
                    next_state <= S1;
                END IF;
            WHEN S1 =>
                IF (x = '0') THEN
                    next_state <= S1;
                END IF;
            when others => next_state <= S0;
        end case;
    end process;

end Behavioral;
