----------------------------------------------------------------------------------
-- Company: Duque y asociados S.L
-- Engineer: Alejandro Garc�a Redondo
-- Create Date: 11.11.2018 20:47:45
-- Design Name: 
-- Module Name: contador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity contador is
Port ( 
    boton : in std_logic;
    cuenta : out std_logic_vector(3 downto 0)
);
end contador;

architecture Behavioral of contador is
    signal cuenta_i : std_logic_vector(cuenta'range);
begin
    process(boton)
    begin
        if rising_edge(boton) then
            cuenta_i <= cuenta_i + 1;
        end if;
    end process;
    cuenta <= cuenta_i;
end Behavioral;
