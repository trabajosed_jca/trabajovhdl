----------------------------------------------------------------------------------
-- Company: Duque y asociados S.A.
-- Engineer: Alejandro Garc�a Redondo
-- 
-- Create Date: 08.12.2018 11:08:03
-- Design Name: 
-- Module Name: semaforo1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity semaforo1 is
    Port ( 
        clk: in std_logic;
        reset: in std_logic;
       -- pulsador: in std_logic;
        rojo: out std_logic;
        ambar: out std_logic;
        verde: out std_logic;
        peatones_v: out std_logic;
        peatones_r: out std_logic
    );
end semaforo1;

architecture Behavioral of semaforo1 is
    type state_type is (S0, S1, S2);
    signal state, next_state: state_type;
    signal temp_v, temp_a, temp_r: std_logic;
    signal cont: integer := 0;
begin
    sync_proc: process (reset, clk)
    begin
        if reset = '1' then
            state <= S0;
        elsif rising_edge(clk) then
            state <= next_state;
        end if;
    end process;
    output_decode: process (state)
    begin
        case (state) is
            when S0 =>  
                verde <= '1';
                rojo <= '0';
                ambar <= '0';
                peatones_v <= '0';
                peatones_r <= '1';                        
            when S1 =>  
                verde <= '0';
                rojo <= '0';
                ambar <= '1';
                peatones_v <= '0';
                peatones_r <= '1';        
            when S2 => 
                verde <= '0';
                rojo <= '1';
                ambar <= '0';
                peatones_v <= '1';
                peatones_r <= '0';
            when others => 
                verde <= '0';
                rojo <= '0';
                ambar <= '0';
                peatones_v <= '0';
                peatones_r <= '0';
        end case;
    end process;
    next_state_decode: process (state, temp_v, temp_a, temp_r)
    begin
        case (state) is
            when S0 =>
                if (temp_v = '1') then
                    next_state <= S1;
                end if;
            when S1 =>
                if (temp_a = '1') then
                    next_state <= S2;
                end if;
            when S2 =>
                if(temp_r = '1') then
                    next_state <= S0;
                end if;
            when others => next_state <= S0;
        end case;
    end process;
    temporizador: process (clk)
    begin
        if reset='1' then
            cont <= 0;
            temp_v <= '0';
            temp_a <= '0';
            temp_r <= '0';
        elsif rising_edge (clk) then
            cont <= cont + 1;
            if (cont = 10) then
                temp_r <= '0';
                temp_v <= '1';
            elsif (cont = 13) then
                temp_v <= '0';
                temp_a <= '1';
            elsif (cont = 18) then
                temp_a <= '0';
                temp_r <= '1';
                cont <= 0;
            end if;
        end if; 
    end process;
end Behavioral;
