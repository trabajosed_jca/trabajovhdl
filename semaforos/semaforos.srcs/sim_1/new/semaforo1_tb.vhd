----------------------------------------------------------------------------------
-- Company: Duque y asociados S.A.
-- Engineer: Alejandro Garc�a Redondo
-- 
-- Create Date: 08.12.2018 11:46:46
-- Design Name: 
-- Module Name: semaforo1_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity semaforo1_tb is
--  Port ( );
end semaforo1_tb;

architecture Behavioral of semaforo1_tb is
    component semaforo1 is
    Port ( 
            clk: in std_logic;
            reset: in std_logic;
           -- pulsador: in std_logic;
            rojo: out std_logic;
            ambar: out std_logic;
            verde: out std_logic;
            peatones_v: out std_logic;
            peatones_r: out std_logic
        );
    end component;
    signal clk: std_logic := '0';
    signal reset, rojo, ambar, verde, peatones_v, peatones_r : std_logic;
begin
    inst_sem1: semaforo1 port map(
        clk => clk,
        reset => reset,
       -- pulsador => pulsador,
        rojo => rojo,
        ambar => ambar,
        verde => verde,
        peatones_v => peatones_v,
        peatones_r => peatones_r
    );
    clk <= not clk after 50 ms;
    reset <= '1' after 0 ns, '0' after 100 ms;
--    process
--    begin
--        pulsador <= '0';
--        wait for 125 ms;
--        pulsador <= '1';
--        wait for 75 ms;
--        pulsador <= '0';
--        wait for 200 ms;
--        pulsador <= '1';
--       wait for 75 ms;
--    end process;
end Behavioral;
